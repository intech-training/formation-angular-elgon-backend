const restify = require('restify'),
	fs 		= require('fs'),
	_ 		= require('lodash'),
	JSON5 	= require('json5');

const pokemonMocks = require("./mocks/pokemons");


const FAKE_AUTH_VALID_TOKEN = "1234";

const server = restify.createServer({
  name: 'mock-api',
  version: '0.0.1'
});

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
//server.use(restify.CORS());
server.use(restify.fullResponse());


function setOptHeaders(res){
	res.setHeader('Access-Control-Allow-Origin', '*');
	//res.setHeader('Access-Control-Allow-Credentials', 'true');
	res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token, Authorization, Content-Disposition, X-Content-Disposition');
	//res.setHeader('Access-Control-Allow-Headers', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE ,POST, PATCH, OPTIONS');
	res.setHeader('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time, Content-Disposition');
	//res.setHeader('Access-Control-Expose-Headers', '*');
	res.setHeader('Access-Control-Max-Age', '1000');
}


function setHeaders(res){
	res.setHeader('Access-Control-Allow-Origin', '*');
	//res.setHeader('Access-Control-Allow-Credentials', 'true');
	res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token, Authorization, Content-Disposition, X-Content-Disposition');
	//res.setHeader('Access-Control-Allow-Headers', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE ,POST, PATCH, OPTIONS');
	res.setHeader('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time, Content-Disposition');
	//res.setHeader('Access-Control-Expose-Headers', '*');
	res.setHeader('Access-Control-Max-Age', '1000');
}



function checkAuth(req){

	const authHeader = req.header('Authorization');

	return (!_.isNil(authHeader)&& authHeader === FAKE_AUTH_VALID_TOKEN);

}

server.opts(/\.*/,  (req, res, next) => {

	setOptHeaders(res)
	res.send(204);
	next();
});

server.get('/v1/rulebook.pdf',  (req, res, next) => {

	if(!checkAuth(req)){
		res.send(401);
		return next();
	} 

	setHeaders(res)


	res.setHeader('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time, Content-Disposition');
	res.setHeader('Content-Disposition', 'attachment; filename=rulebook.pdf')
	fs.readFile(__dirname + '/mocks/TCG-rulebook-en.pdf', (err, data) => {
		if (err) {
				next(err);
				return;
		}
		res.setHeader('Content-Type', 'application/pdf');
		res.writeHead(200);
		res.end(data);
		next();
	});


});

server.get('/application/isAlive',  (req, res, next) => {

	setHeaders(res)
	
	res.send(200, true);
  
  	return next();
});


server.get('/v1/cards/',  (req, res, next) => {

	if(!checkAuth(req)){
		res.send(401);
		return next();
	} 
	setHeaders(res)
	res.send(200, pokemonMocks.pokemons);
  
	return next();

});


server.get('/v1/cards/:cardId',  (req, res, next) => {
	if(!checkAuth(req)){
		res.send(401);
		return next();
	} 

	setHeaders(res)
	
	const cardId = req.params.cardId;
	const maybeAccount = _.find( pokemonMocks.pokemons.cards, p => p.id === cardId);

	(maybeAccount)?
		res.send(200, maybeAccount) :
		res.send(404, null);
  
		return next();
});

server.listen(9004,  () => {
  console.log('%s listening at %s', server.name, server.url);
});